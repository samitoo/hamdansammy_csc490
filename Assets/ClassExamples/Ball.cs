using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	
	public TextMesh leftScoreTextMesh;
	public TextMesh rightScoreTextMesh;
	
	public float speed = 10;
	
	private int leftScore = 0;
	private int rightScore = 0;
	
	// Use this for initialization
	void Start () {
		rigidbody.velocity = Vector3.right * speed;
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 v = rigidbody.velocity;
		Vector3 topRight = Camera.main.WorldToViewportPoint (transform.position + collider.bounds.extents);
		Vector3 bottomLeft = Camera.main.WorldToViewportPoint (transform.position - collider.bounds.extents);
		
		Vector3 pos = transform.position;
		Vector2 camPos = Camera.main.transform.position;
		
		//ball hits top or bottom of screen
		if (topRight.y > 1) {
			rigidbody.velocity = new Vector3(v.x -Mathf.Abs (v.y),v.z);
		}
		else if (bottomLeft.y < 0) {
			rigidbody.velocity = new Vector3(v.x, Mathf.Abs (v.y), v.z);
		}
		
		//ball goes off sides of screen
		else if (bottomLeft.x>1) {
			leftScore++;
			leftScoreTextMesh.text = leftScore.ToString ();
			transform.position = new Vector3(camPos.x, camPos.y, pos.z);
			rigidbody.velocity = Vector3.left * speed;
			
		}
		else if (topRight.x<0){
			rightScore++;
			rightScoreTextMesh.text = rightScore.ToString ();
			transform.position = new Vector3(camPos.x, camPos.y, pos.z);
			rigidbody.velocity = Vector3.right * speed;
		}
		
	
	}
}
