using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(CapsuleCollider))]
public class Motor : MonoBehaviour {
	
	public float speed = 1.0f;
	public float maxSpeedChange = 1.0f;
	public float jumpHeight = 1.0f;
	
	
	
	[HideInInspector]
	public bool shouldJump = false;
	[HideInInspector]
	public Vector3 targetVelocity = Vector3.zero;
	
	bool grounded = false;
	
	Vector3 contactNorm = Vector3.zero;
	
	public bool OnGround() {
		return grounded;
	}

	public bool InAir() {
		return !grounded;
	}
	
	
	void FixedUpdate () {
		
		Vector3 velocity = rigidbody.velocity;
		Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange = velocityChange.normalized * Mathf.Clamp (velocityChange.magnitude, -0, maxSpeedChange);
		velocityChange.y = 0; 
		
		
		if (shouldJump && grounded) {
			rigidbody.velocity = new Vector3 (velocity.x, CalculateJumpVerticalSpeed(),velocity.z);
		}
		
		rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
				
		grounded = false;		
	
	}
	
	void OnCollisionStay (Collision collision) {
		
		foreach (ContactPoint cp in collision.contacts){
			contactNorm = cp.normal;
//			float norm = Mathf.Rad2Deg * Mathf.Atan2 (contactNorm.y, Mathf.Abs (contactNorm.x));
			if (contactNorm.y > Mathf.Abs(contactNorm.x)) {
				grounded = true;
				}
			
		}
		
//		if (collision.gameObject.layer == LayerMask.NameToLayer("env")){
//		}	
		
		
		
	}
	
	float CalculateJumpVerticalSpeed (){
		float g = Physics.gravity.y;
		return Mathf.Sqrt(2 * jumpHeight * -g);
	}
}
