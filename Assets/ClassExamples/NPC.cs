using UnityEngine;
using System.Collections;

public class NPC : MonoBehaviour {
	
	public float minDiff =2;
	public float speed = 10;
	public float acceleration = 10;
	
	private float steering = 0;
	private Ball ball;
	

	// Use this for initialization
	void Start () {
		ball = (Ball)FindObjectOfType (typeof(Ball));
	
	}
	
	// Update is called once per frame
	void Update () {
			float newSteering = 0;
			if (transform.position.y - minDiff > ball.transform.position.y) {
				newSteering = -1;
				}
			else if (transform.position.y + minDiff < ball.transform.position.y) {
				newSteering = 1;
			}
			steering = Mathf.Lerp (steering, newSteering, Time.deltaTime * acceleration);
			rigidbody.velocity = Vector3.up * steering * speed;
	
	}
}
