using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Motor))]
public class PlatformPlayerController : MonoBehaviour {
	
	public float speed = 1;
	Motor motor;

	// Use this for initialization
	void Start () {
		
		motor = GetComponent<Motor>();
	
	}
	
	// Update is called once per frame
	void Update () {
		
		motor.targetVelocity.x = Input.GetAxis ("Horizontal") * speed;
		motor.shouldJump = Input.GetButton ("Fire1");
	
	}
}
