using UnityEngine;
using System.Collections;

public class BallLaunch : MonoBehaviour {
	
	private float speed = 10;
	public TextUpdate lives;
	
	void Update () {
		
		Vector3 v = rigidbody.velocity;
		Vector3 top = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		Vector3 bottom = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
		
		if (bottom.y > 1) {
			rigidbody.velocity = new Vector3(v.x, -Mathf.Abs(v.y), v.z);
        }
        else if (top.y < 0) {
			Reset();
			lives.Death();
        }
		else if (bottom.x > 1){
			rigidbody.velocity = new Vector3(-Mathf.Abs(v.x), v.y, v.z);
		}
		else if (top.x < 0) {
			rigidbody.velocity = new Vector3(Mathf.Abs(v.x), v.y, v.z);
		}
		
		
		if(Input.GetButton("Jump")) {
			rigidbody.velocity = Vector3.up * speed;		
		}
	}
	
	void Reset (){
		transform.position = new Vector3(-0.1235f, 6.506f, 1.217f);
		rigidbody.velocity = new Vector3(0, 0, 0);
	}
	
	void OnCollisionExit(Collision collision) {
		rigidbody.velocity = rigidbody.velocity.normalized * speed;
	}
	
	
	
	
}
