using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {
	
	public TextUpdate totalScore;
	private int hit = 0;
		
	void OnCollisionEnter (Collision Collision) {
		hit++;
		
		if ( hit == 2){
		Destroy(gameObject);
		totalScore.ScoreIncrease () ;	
		}
	}
}
