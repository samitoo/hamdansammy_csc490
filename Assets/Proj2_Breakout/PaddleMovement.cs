using UnityEngine;
using System.Collections;

public class PaddleMovement : MonoBehaviour {
	
	private float speed = 10;
	
	void FixedUpdate () {
		rigidbody.position += Vector3.right * Input.GetAxis ("Horizontal") * Time.deltaTime * speed;
	}
}
