using UnityEngine;
using System.Collections;

public class TextUpdate : MonoBehaviour {
	
	private int score = 0;
	private int lives = 3;
	private int numBlocks = 25;
	public TextMesh scoreText;
	public TextMesh life;
	public TextMesh highScoreMesh;
	private int highScore = 1;
	
	void Start () {
		life.text = lives.ToString();
	}
	void fixedUpdate (){
		highScore = PlayerPrefs.GetInt("HighScore");
	}
	
	public void ScoreIncrease (){
		score++;
		scoreText.text = score.ToString();
		
		if (score >= numBlocks){
			scoreText.text = "You Win!";
			}
		
		if (score > highScore){
			PlayerPrefs.SetInt("HighScore", score);
			PlayerPrefs.Save();
			highScore = score;
			highScoreMesh.text = highScore.ToString();
			}
		
	}
	public void Death(){
		lives--;
		
		if (lives > 0){
		life.text = lives.ToString();
		}
		else {
			life.text = "Game Over";
		}
		
		if(lives == 0){
			Application.LoadLevel(0);
		}
	}
}
	

